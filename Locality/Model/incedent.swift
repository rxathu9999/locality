//
//  incedent.swift
//  Locality
//
//  Created by Thuc Truong on 6/29/15.
//  Copyright (c) 2015 peerbitVN. All rights reserved.
//

import Foundation
import MapKit
import Parse
enum incedentType:String {
    case TrafficIncidents = "Traffic Incidents"
    case Works = "Works"
}

class incedent: NSObject, MKAnnotation {
    //Anotation Property
    var title: String = ""
    var locationName: String = ""
    var coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D()
    
    //

    var type: incedentType = incedentType.TrafficIncidents
    var category: String = ""
    var comments: String = ""
    var corridor: String = ""
    var createdAt: NSDate = NSDate()
    var currentUpdate : String = ""
    var displayPoint: PFGeoPoint = PFGeoPoint()
    var disruptionId: String = ""
    var endTime: NSDate = NSDate()
    var lastModTime:NSDate = NSDate()
    var levelOfInterest:String = ""
    var location: String = ""
    var objectId: String = ""
    var remarkTime:NSDate = NSDate()
    var severity:String = ""
    var startTime: NSDate = NSDate()
    var status:String = ""
    var subCategory:String = ""
    var updatedAt: NSDate = NSDate()
    
    override init() {
        super.init()
        
    }

    init(title: String, locationName: String, displayPoint: PFGeoPoint) {
        self.category = title
        self.title = self.category
        
        self.location = locationName
        self.locationName = self.location
        
        self.coordinate = CLLocationCoordinate2D(latitude: displayPoint.latitude, longitude: displayPoint.longitude)
        
    }
    
    var subtitle: String {
        return locationName
    }
    
}
   

