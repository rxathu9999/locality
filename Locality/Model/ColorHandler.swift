



import UIKit
import Foundation

private let _sharedDataCenter = ColorHandler()
class ColorHandler: NSObject {
    
    class var sharedInstance: ColorHandler {
        return _sharedDataCenter
    }

    func CriticalColor() -> UIColor{
        
      return UIColor(red: 245/255, green: 73/255, blue: 73/255, alpha: 1)
    }
    func EmergencyColor() -> UIColor{
        return UIColor(red: 32/255, green: 113/255, blue: 214/255, alpha: 1)
    }
    func TravelColor() -> UIColor{
       return UIColor(red: 247/255, green: 173/255, blue: 14/255, alpha: 1)
    }
    func WeatherColor() -> UIColor{
       return UIColor(red: 0, green: 189/255, blue: 244/255, alpha: 1)
    }
    func EventsColor() -> UIColor{
       return UIColor(red: 141/255, green: 198/255, blue: 63/255, alpha: 1)
    }
    
    
    

}
