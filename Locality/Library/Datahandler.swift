

import UIKit
import Parse
class Datahandler: NSObject {
    
    func loadJsonData(JsonFileName:String) -> AnyObject{
        let fileName = NSBundle.mainBundle().pathForResource(JsonFileName, ofType: "json");
        var readError : NSError?
        var jsonData: NSData = NSData(contentsOfFile: fileName!, options: NSDataReadingOptions.DataReadingMappedIfSafe, error: &readError)!
        var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary
        var result: AnyObject? = jsonResult["results"]
        return result!
    }
    func loadParseData(){
        let testObject = PFQuery(className: "Incident")    
        testObject.findObjectsInBackgroundWithBlock { (data :[AnyObject]?, error: NSError?) -> Void in
            if error == nil {
                for d  in (data as! [PFObject]){
                    println(d["category"])
                }
                
            }else {
                println(error)
            }
        }
        
    }
// calling below method in applicationDidFinishLaunching method of AppDelegate, pass database name as argument in copyFile method.
// class func copyFile(fileName: NSString) {
//        var dbPath: NSString = getPath(fileName as String)
//        var fileManager = NSFileManager.defaultManager()
//        if !fileManager.fileExistsAtPath(dbPath as String) {
//            var fromPath: NSString = NSBundle.mainBundle().resourcePath!.stringByAppendingPathComponent(fileName as String)
//            fileManager.copyItemAtPath(fromPath as String, toPath: dbPath as String, error: nil)
//        }
//    }
 
}
