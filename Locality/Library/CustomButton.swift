//
//  CustomButton.swift



import UIKit

class CustomButton: UIButton {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    */
    override func drawRect(rect: CGRect) {
        // Drawing code
        
        self.setRightImage()
    }
    func setBackgroundColor(color: UIColor, forUIControlState state: UIControlState) {
        self.setBackgroundImage(imageWithColor(color), forState: state)
    }
//    override var highlighted: Bool {
//        get {
//            return super.highlighted
//        }
//        set {
//            if newValue {
//                backgroundColor = UIColor.blackColor()
//            }
//            else {
//                backgroundColor = UIColor.whiteColor()
//            }
//            super.highlighted = newValue
//        }
//    }
    
    func setRightImage()
    {
//        self.layer.borderWidth = 0.5
//        self.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        let image = UIImage(named: "alert") as UIImage?
        let button   = UIImageView()
        button.frame = CGRectMake(0, 0, 20, 20)
        button.image = image
        var f = button.frame
        f.origin.y = self.frame.size.height/2 - f.size.height / 2
        f.origin.x = self.frame.width + 5 - f.size.width //- 5
        
        button.frame = f
        
        self.addSubview(button)
        
    }
    func setleftImage(imagename: String, w:CGFloat, h:CGFloat)
    {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(red: 46/255, green: 61/255, blue: 64/255, alpha: 1).CGColor

        
        let image = UIImage(named: imagename) as UIImage?
        let button   = UIImageView()
        button.frame = CGRectMake(0, 0, w, h)
        button.image = image
        var f = button.frame
        f.origin.y = self.frame.size.height/2 - f.size.height / 2
        f.origin.x = 10 //self.frame.width - f.size.width - 10
        
        button.frame = f
        
        self.addSubview(button)
        
    }
    private func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRectMake(0.0, 0.0, 1.0, 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        CGContextSetFillColorWithColor(context, color.CGColor)
        CGContextFillRect(context, rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }


}
