//
//  FeedbackViewController.swift
//  Locality
//
//  Created by Thuc Truong on 7/9/15.
//  Copyright (c) 2015 peerbitVN. All rights reserved.
//

import UIKit
import MessageUI
class FeedbackViewController: UIViewController,MFMailComposeViewControllerDelegate {
    @IBOutlet weak var inputComment: UITextView!
    @IBOutlet weak var inputEmail: UITextField!
    @IBOutlet weak var btnSend: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        let leftButton = UIBarButtonItem(image: UIImage(named: "back_arrow"), style: UIBarButtonItemStyle.Done, target: self, action: "pressedleft")
            leftButton.tintColor = UIColor.whiteColor()
        self.navigationItem.leftBarButtonItem = leftButton
        //
        btnSend.layer.cornerRadius = 5
        inputComment.layer.borderWidth = 0.5
        inputComment.layer.borderColor = UIColor.lightGrayColor().CGColor
        inputComment.layer.cornerRadius = 5
        
    }
    func pressedleft(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func touchSend(sender: UIButton) {
        if inputComment.text.isEmpty {
            let sendMailErrorAlert = UIAlertView(title: "Feedback and Message content", message: "Your feedback is empty", delegate: self, cancelButtonTitle: "OK")
            sendMailErrorAlert.show()
            return
        }
        
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["shivangi@spaceotechnologies.com"])
        mailComposerVC.setSubject("Locality app feedback")
        mailComposerVC.setMessageBody(self.inputComment.text, isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        if error == nil {
            controller.dismissViewControllerAnimated(true, completion: nil)
        } else {
            println(error.description)
                let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
                sendMailErrorAlert.show()
        
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
