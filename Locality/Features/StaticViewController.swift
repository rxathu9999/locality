//
//  StaticViewController.swift
//  Locality
//
//  Created by Thuc Truong on 7/8/15.
//  Copyright (c) 2015 peerbitVN. All rights reserved.
//

import UIKit

class StaticViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let leftButton = UIBarButtonItem(image: UIImage(named: "close"), style: UIBarButtonItemStyle.Done, target: self, action: "pressed")
        leftButton.tintColor = UIColor.whiteColor()
        self.navigationItem.leftBarButtonItem = leftButton
    }
    func pressed(){
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool) {
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
