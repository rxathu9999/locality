//
//  LocalityTableViewCell.swift
//  Locality
//
//  Created by Thuc Truong on 6/30/15.
//  Copyright (c) 2015 peerbitVN. All rights reserved.
//

import UIKit

class LocalityTableViewCell: UITableViewCell {
    @IBOutlet weak var imageTitlte: UIImageView!
    @IBOutlet weak var lbtitle: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet weak var lbType: UILabel!
    @IBOutlet weak var lbDescription: UILabel!
    @IBOutlet weak var btnMore: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
