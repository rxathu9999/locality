//
//  IncedentTableViewCell.swift
//  Locality
//
//  Created by Thuc Truong on 6/30/15.
//  Copyright (c) 2015 peerbitVN. All rights reserved.
//

import UIKit

class IncedentTableViewCell: UITableViewCell {
    @IBOutlet weak var lbDateCreate: UILabel!
    @IBOutlet weak var imgTitle: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSubTitle: UILabel!
    @IBOutlet weak var lbComment: UILabel!
    @IBOutlet weak var lbLastUpdate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
