

import UIKit
import MapKit
import Parse
class IncedentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var mapheight: NSLayoutConstraint!
    
    var incedentData:incedent!
    var BoundaryPoint = [PFGeoPoint]()
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.view.frame.size.width == 375 || self.view.frame.size.width == 414 {
            mapheight.constant = 300
        }
        
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableViewAutomaticDimension
        // setting NavigationBar
        self.title = "INCIDENT VIEW"
        let leftButton = UIBarButtonItem(image: UIImage(named: "back_arrow"), style: UIBarButtonItemStyle.Done, target: self, action: "pressedleft")
        let rightButton = UIBarButtonItem(image: UIImage(named: "settings"), style: UIBarButtonItemStyle.Done, target: self, action: "pressedright")
        leftButton.tintColor = UIColor.whiteColor()
        rightButton.tintColor = UIColor.whiteColor()
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.rightBarButtonItem = rightButton
        // show incedent on map
        mapView.addAnnotation(incedentData)
        //zoom in Anotation
        centerMapOnLocation(incedentData.coordinate)
        //load Polyline
        LoadboundaryByObj()
    }
    func pressedleft(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    func pressedright(){
        var vc = self.storyboard?.instantiateViewControllerWithIdentifier("SettingTableViewController") as! SettingTableViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func LoadboundaryByObj(){

        let Obj = PFQuery(className: "Incident_Boundary")
        Obj.findObjectsInBackgroundWithBlock { (data :[AnyObject]?, error: NSError?) -> Void in
            if error == nil {
                for point in data as! [PFObject]{
                    
                    if self.incedentData.objectId == point["incident"]?.objectId {
                        self.BoundaryPoint.append(point["pointN"] as! PFGeoPoint)
                    }
                }
                if self.BoundaryPoint.count != 0 {
                    var points =  [CLLocationCoordinate2D]()
                    for coor in self.BoundaryPoint {
                        points.append(CLLocationCoordinate2D(latitude: coor.latitude, longitude: coor.longitude))
                    }
                    var geodesic = MKGeodesicPolyline(coordinates: &points[0], count: points.count)
                    self.mapView.addOverlay(geodesic)
                
                }
            }else {
                println(error)
            }
        }
    }
    //ZoomIn
    let regionRadius: CLLocationDistance = 400
    
    func centerMapOnLocation(location: CLLocationCoordinate2D) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location,
            regionRadius , regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    // MARK: - Map Deligate
    func mapView(mapView: MKMapView!, didUpdateUserLocation userLocation: MKUserLocation!) {
        
    }
    // MARK: - Map view data source

    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKPolyline {
            var polylineRenderer = MKPolylineRenderer(overlay: overlay)
            var stokeCL:UIColor!
            switch incedentData.category {
            case "Critical":
                stokeCL = ColorHandler.sharedInstance.CriticalColor()
            case "Emergency Services":
                stokeCL = ColorHandler.sharedInstance.EmergencyColor()
            case "Travel":
                stokeCL = ColorHandler.sharedInstance.TravelColor()
            case "Weather":
                stokeCL = ColorHandler.sharedInstance.WeatherColor()
            case "Events":
                stokeCL = ColorHandler.sharedInstance.EventsColor()
            default:
                stokeCL = UIColor.blackColor()
            
            }
                polylineRenderer.strokeColor = stokeCL
            
            polylineRenderer.lineWidth = 10
            return polylineRenderer
        }
        
        return nil
    }
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if let annotation = annotation as? incedent {
            let identifier = "incedent"
            var view: MKPinAnnotationView
            
            if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
                as? MKPinAnnotationView { // 2
                    dequeuedView.annotation = annotation
                    view = dequeuedView
            } else {
                // 3
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: 0, y: 0)
                view.image = UIImage(named: "police_car")
                
            }
            return view
        }
        return nil
        
    }

    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 2
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableViewAutomaticDimension
        }else {
            return 150
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! IncedentTableViewCell
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "EEE, dd MMM yyyy '-' HH:mm"
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
            dateFormatter.timeZone = NSTimeZone(name: "Europe/London")
            cell.sizeToFit()
            cell.lbDateCreate.text = "Started " +  dateFormatter.stringFromDate(incedentData.createdAt)
            cell.lbTitle.text = incedentData.category
            cell.lbSubTitle.text = incedentData.subCategory
            cell.lbComment.text = incedentData.comments
            println(incedentData.comments)
            dateFormatter.dateFormat = "dd MMM yyyy '-' HH:mm"
            cell.lbLastUpdate.text = "Last updated " + dateFormatter.stringFromDate(incedentData.updatedAt)
        
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("additiondataCell", forIndexPath: indexPath) as! additiondataCell
            cell.backgroundColor = UIColor.clearColor()
            cell.sizeToFit()
            cell.lbAdditionComment.text = incedentData.currentUpdate
            
            return cell
        }
    }

    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
    }
    @IBAction func touchShare(sender: UIButton) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy '-' HH:mm"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.timeZone = NSTimeZone(name: "Europe/London")
        let createTime = dateFormatter.stringFromDate(incedentData.createdAt)
        var emailItem = EmailItemProvider()
            emailItem.subject = incedentData.title + " " + incedentData.subtitle
            emailItem.body = incedentData.title + " \n " + incedentData.subtitle + "\n" + createTime + "\n" + incedentData.comments
        let activityItems = [incedentData.title,incedentData.subtitle,createTime,"----------",incedentData.comments]
        let avc = UIActivityViewController(activityItems: [emailItem] as [AnyObject], applicationActivities: nil)
        
        self.presentViewController(avc, animated: true) { () -> Void in
        }
        

    }


}
class EmailItemProvider: NSObject,UIActivityItemSource {
    var subject:String = ""
     var body:String = ""
    func activityViewControllerPlaceholderItem(activityViewController: UIActivityViewController) -> AnyObject {
         return subject
    }
    func activityViewController(activityViewController: UIActivityViewController, itemForActivityType activityType: String) -> AnyObject? {
        return body
    }// called to fetch data after an activity is selected. you can return nil.
    
    func activityViewController(activityViewController: UIActivityViewController, subjectForActivityType activityType: String?) -> String {
        return subject
    }// if activity supports a Subject field. iOS 7.0
//    optional func activityViewController(activityViewController: UIActivityViewController, dataTypeIdentifierForActivityType activityType: String?) -> String {
//    
//    }// UTI for item if it is an NSData. iOS 7.0. will be called with nil activity and then selected activity
//    optional func activityViewController(activityViewController: UIActivityViewController, thumbnailImageForActivityType activityType: String!, suggestedSize size: CGSize) -> UIImage! {
//    
//    }// if activity supports preview image. iOS 7.0

}