//
//  LocalityTableViewController.swift
//  Locality
//
//  Created by Thuc Truong on 6/26/15.
//  Copyright (c) 2015 peerbitVN. All rights reserved.
//

import UIKit
import MapKit
import AddressBook
import Parse

class LocalityTableViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,MKMapViewDelegate,CLLocationManagerDelegate{
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var mapheight: NSLayoutConstraint!
    let regionRadius: CLLocationDistance = 500 // define Zoom Radius
    let AlertRadius : CLLocationDistance = 500
    let initialLocation = CLLocationCoordinate2D(latitude: 51.509580, longitude: -0.134046) // set initial location
    var incedentPoints = [incedent]()
    var incidentsSort = [incedent]()
    var messIsShow = false
    var notificationIsShow = false
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.layer.cornerRadius = 5
        if self.view.frame.size.width == 375 || self.view.frame.size.width == 414{
            mapheight.constant = 300
        }
        let rightButton = UIBarButtonItem(image: UIImage(named: "settings"), style: UIBarButtonItemStyle.Done, target: self, action: "pressedright")
        rightButton.tintColor = UIColor.whiteColor()
        self.navigationItem.rightBarButtonItem = rightButton
        
        locationManager.delegate = self
        
        
   
    }
    func pressedright(){
        var vc = self.storyboard?.instantiateViewControllerWithIdentifier("SettingTableViewController") as! SettingTableViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func formatterDateToLocalTime(input: NSDate) -> String{
        let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "h:mm a" // format to local time
            //dateFormatter.timeZone =
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
            dateFormatter.timeZone = NSTimeZone(name: "Europe/London")
        return dateFormatter.stringFromDate(input)
    }
    //Load Annotation
    func loadMapAnno(){
        let testObject = PFQuery(className: "Incident")
        //        testObject.whereKey("displayPoint", nearGeoPoint: PFGeoPoint(location: initialLocation))
        testObject.orderByAscending("createdAt")
        testObject.limit = 1000
        testObject.findObjectsInBackgroundWithBlock { (data :[AnyObject]?, error: NSError?) -> Void in
            if error == nil {

            if self.incedentPoints.count == 0 {
                for point  in (data as! [PFObject]){
                    println(point)
                    var Singleincedent = incedent(title: point["category"] as! String,
                        locationName: point["location"] as! String,
                        displayPoint: point["displayPoint"] as! PFGeoPoint)
                    
                    Singleincedent.comments = point["comments"] as! String
                    Singleincedent.currentUpdate = point["currentUpdate"] as! String
                    Singleincedent.createdAt = point.createdAt!
                    Singleincedent.updatedAt = point.updatedAt!
                    Singleincedent.lastModTime = point["lastModTime"] as! NSDate
                    Singleincedent.severity = point["severity"] as! String
                    Singleincedent.levelOfInterest = point["levelOfInterest"] as! String
                    Singleincedent.objectId = point.objectId!
                    Singleincedent.remarkTime = point["remarkTime"] as! NSDate
                    Singleincedent.status = point["status"] as! String
                    Singleincedent.subCategory = point["subCategory"] as! String
                    if Singleincedent.status == "Active" {
                        self.incedentPoints.append(Singleincedent)
                    }
                }
                self.mapView.addAnnotations(self.incedentPoints)
                self.tableView.reloadData()
            }
                //Check and Push Alert
                if !self.notificationIsShow {
                for ince in self.incedentPoints {
                    println(self.updateDistanceToAnnotation(ince.coordinate))
                    if self.updateDistanceToAnnotation(ince.coordinate) <  self.AlertRadius {
                        let distance = String(format: "\nDistance %4.0f m", self.updateDistanceToAnnotation(ince.coordinate))
                        let mess = ince.subCategory
                        //localNotification
                        var localNotification:UILocalNotification = UILocalNotification()
                        localNotification.alertAction = ince.title
                        localNotification.alertBody = mess + "\(distance) from you"
                        localNotification.fireDate = NSDate(timeIntervalSinceNow: 1)
                        localNotification.soundName = UILocalNotificationDefaultSoundName
                        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                        //
                        let alertController = UIAlertController(title: ince.title, message: mess + "\(distance) from you", preferredStyle: UIAlertControllerStyle.Alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
                        self.presentViewController(alertController, animated: true, completion: nil)
                        break
                    }
                }
                  self.notificationIsShow = true
                }
            }else {
                println(error)
            }
        }
    }

    

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationAuthorizationStatus()
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if tableView.indexPathForSelectedRow() != nil {
            tableView.deselectRowAtIndexPath(tableView.indexPathForSelectedRow()!, animated: true)
        }

    }
    func LondonAuthentication(){
        if messIsShow {
            return
        }
        if mapView.userLocation.location == nil {
            return
        }
        CLGeocoder().reverseGeocodeLocation(mapView.userLocation.location) { (placemarks, error) -> Void in
            if (error != nil) {
                println("reverse geodcode fail: \(error.localizedDescription)")
            } else {
                let pm = placemarks as! [CLPlacemark]
                println(pm[0].addressDictionary)
                
                if pm[0].locality != nil{
                    
                    if  pm[0].locality == "London" {
                        // you r in London
                        
                    }else {
                        //Push Notification if not in LONDON
                        AZNotification.showNotificationWithTitle("     The Application is London only for now", controller: self, notificationType: AZNotificationType.Message,isHiden: true)
                    }
                } else {
                    //Push Notification if not in LONDON
                    AZNotification.showNotificationWithTitle("     The Application is London only for now", controller: self, notificationType: AZNotificationType.Message,isHiden: true)
                }
            }
        }
        messIsShow = true
    }
    
    //MARK:- location manager
    var locationManager = CLLocationManager()
    func checkLocationAuthorizationStatus() {

        switch CLLocationManager.authorizationStatus()
        {
        case .AuthorizedWhenInUse:
            mapView.showsUserLocation = true
        case .AuthorizedAlways:
            mapView.showsUserLocation = true
        default: locationManager.requestWhenInUseAuthorization()
        }

    }
    func locationManager(manager: CLLocationManager!, didUpdateToLocation newLocation: CLLocation!, fromLocation oldLocation: CLLocation!) {
        var currentLocation = newLocation
        
        
        if  incedentPoints.count != 0{
        
            for ince in self.incedentPoints {
                //println(self.updateDistanceToAnnotation(ince.coordinate))
                if self.distanceLtoL(ince.coordinate, L2: currentLocation.coordinate) <  self.AlertRadius {
                    let distance = String(format: "\n--Distance %4.0f m", self.updateDistanceToAnnotation(ince.coordinate))
                    let mess = ince.subCategory
                    //localNotification
                    var localNotification:UILocalNotification = UILocalNotification()
                    localNotification.alertAction = ince.title
                    localNotification.alertBody = mess + "\(distance) from you---"
                    localNotification.fireDate = NSDate(timeIntervalSinceNow: 1)
                    localNotification.soundName = UILocalNotificationDefaultSoundName
                    UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                    //
                    break
                }
            }
        }
        
        
    }
    //ZoomIn
    func centerMapOnLocation(location: CLLocationCoordinate2D) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location,
            regionRadius , regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    //Distance to UserLocation
    func updateDistanceToAnnotation(coor:CLLocationCoordinate2D) -> Double{
        
        var userLocation = CLLocation(latitude: mapView.userLocation.coordinate.latitude, longitude: mapView.userLocation.coordinate.longitude)
        var distance = CLLocation(latitude: coor.latitude, longitude: coor.longitude).distanceFromLocation(userLocation)
        return distance as Double
    }
    //


// MARK: - Map view Delegate
    func mapView(mapView: MKMapView!, didUpdateUserLocation userLocation: MKUserLocation!) {
        

        CLGeocoder().reverseGeocodeLocation(userLocation.location) { (placemarks, error) -> Void in
            if (error != nil) {
                println("reverse geodcode fail: \(error.localizedDescription)")
            } else {
                let pm = placemarks as! [CLPlacemark]
                println(pm[0].addressDictionary)
                
                if pm[0].locality != nil{
                    
                    if  pm[0].locality == "London" || pm[0].country == "India"{
                        // you r in London
                    self.mapView.setUserTrackingMode(MKUserTrackingMode.Follow, animated: true)
                    self.loadMapAnno()
        
                    }else {
                        //Push Notification if not in LONDON
                        self.mapView.setUserTrackingMode(MKUserTrackingMode.None, animated: true)
                        self.centerMapOnLocation(self.initialLocation)
                        if !self.messIsShow {
                            
                    AZNotification.showNotificationWithTitle("     The Application is London only for now", controller: self, notificationType: AZNotificationType.Message,isHiden: true)
                            self.messIsShow = true
                        }
                        self.incedentPoints.removeAll(keepCapacity: false)
                        self.tableView.reloadData()
                    }
                } else {
                    //Push Notification if not in LONDON
                    self.mapView.setUserTrackingMode(MKUserTrackingMode.None, animated: true)
                    self.centerMapOnLocation(self.initialLocation)
                    if !self.messIsShow {
                        
                    AZNotification.showNotificationWithTitle("     The Application is London only for now", controller: self, notificationType: AZNotificationType.Message,isHiden: true)
                        self.messIsShow = true
                    }
                    self.incedentPoints.removeAll(keepCapacity: false)
                    self.tableView.reloadData()
                    
                }
            }
        }
        
    }
    
    func distanceLtoL(L1:CLLocationCoordinate2D,L2:CLLocationCoordinate2D) -> Double{
        var distance = CLLocation(latitude: L1.latitude, longitude: L1.longitude).distanceFromLocation(CLLocation(latitude: L2.latitude, longitude: L2.longitude))
        return distance as Double
    }
    
    func mapView(mapView: MKMapView!, regionDidChangeAnimated animated: Bool) {
        incidentsSort.removeAll(keepCapacity: false)
        if incedentPoints.count != 0{
            for inci in incedentPoints {
                if distanceLtoL(inci.coordinate, L2: mapView.centerCoordinate) < 1000 {
                    incidentsSort.append(inci)
                }
            }
          self.tableView.reloadData()
        }
        
    }
    
    func mapViewDidFinishRenderingMap(mapView: MKMapView!, fullyRendered: Bool) {
        //LondonAuthentication()
    }

    var index = 0
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, calloutAccessoryControlTapped control: UIControl!) {
        
        if index == incedentPoints.count{
            index = 0
        }
        centerMapOnLocation(incedentPoints[index].coordinate)
        index++
        
    }
// MARK: - Map view data Source
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if let annotation = annotation as? incedent {
            let identifier = "incedent"
            var view: MKPinAnnotationView
            
            if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
                as? MKPinAnnotationView { // 2
                    dequeuedView.annotation = annotation
                    view = dequeuedView
            } else {
                // 3
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
//                view.centerOffset = CGPoint(x: 0, y: 0)
                view.image = UIImage(named: "police_car")
                let Mybutton = UIButton()
                Mybutton.frame = CGRectMake(0,0, 40, 40)
                Mybutton.addTarget(self, action: nil, forControlEvents: .TouchUpInside)
                Mybutton.setImage(UIImage(named: "disclosure"), forState: UIControlState.Normal)
                view.rightCalloutAccessoryView = Mybutton as UIView
            }
            return view
        }
        return nil

    }

    // MARK: - Table view data source

     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.

        if incedentPoints.count != 0 {
            if incidentsSort.count == 0 {
                        return 1
            }else {
                return incidentsSort.count
            }
        } else {
            return 1
        }
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 117
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! LocalityTableViewCell
        if incedentPoints.count != 0 {

            if incidentsSort.count == 0 {
            
                cell.textLabel?.hidden = false
                cell.textLabel?.text = "Currently no incident to display."
                cell.textLabel?.textColor = UIColor(red: 103/255, green: 103/255, blue: 103/255, alpha: 1)
                cell.selectionStyle = .None

            } else {
                cell.textLabel?.hidden = true
                cell.imageTitlte.hidden = false
                cell.lbtitle.hidden = false
                cell.lbTime.hidden = false
                cell.lbType.hidden = false
                cell.btnMore.hidden = false
                cell.selectionStyle = .Default
                cell.btnMore.tag = indexPath.row
                cell.lbtitle.text = incidentsSort[indexPath.row].category
                cell.lbTime.text =  formatterDateToLocalTime(incidentsSort[indexPath.row].createdAt)
                cell.lbType.text =   incidentsSort[indexPath.row].subCategory
                cell.lbDescription.text = incidentsSort[indexPath.row].comments

            }
        } else {
            
            cell.textLabel?.hidden = false
            cell.textLabel?.text = "Currently no incident to display."
            cell.selectionStyle = .None
            
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("IncedentViewController") as! IncedentViewController
        if incedentPoints.count != 0 {
            if incidentsSort.count == 0 {
            }else {
                vc.incedentData = incidentsSort[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }

    @IBAction func touchMore(sender: UIButton) {
       println(sender.tag)
        if incedentPoints.count != 0 {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("IncedentViewController") as! IncedentViewController
            if incidentsSort.count == 0 {
                vc.incedentData = incedentPoints[sender.tag]
            }else {
                vc.incedentData = incidentsSort[sender.tag]
            }
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}
