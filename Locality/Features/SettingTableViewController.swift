//
//  SettingTableViewController.swift
//  Locality
//
//  Created by Thuc Truong on 6/29/15.
//  Copyright (c) 2015 peerbitVN. All rights reserved.
//

import UIKit

class SettingTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        let leftButton = UIBarButtonItem(image: UIImage(named: "close"), style: UIBarButtonItemStyle.Done, target: self, action: "pressed")
        leftButton.tintColor = UIColor.whiteColor()
        self.navigationItem.leftBarButtonItem = leftButton
    }
    func pressed(){
        var vc = self.storyboard?.instantiateViewControllerWithIdentifier("homeNavigation") as! UINavigationController
        var root = vc.viewControllers[0] as! LocalityTableViewController
        root.messIsShow = true
        self.presentViewController(vc, animated: true, completion: nil)
        //self.navigationController?.popToRootViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 5
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row {
        case 0 :
            println(indexPath.row)
            var vc = self.storyboard?.instantiateViewControllerWithIdentifier("AlertSettingTableViewController") as! AlertSettingTableViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case 1 :
            println(indexPath.row)
            var vc = self.storyboard?.instantiateViewControllerWithIdentifier("StaticViewController") as! StaticViewController
                vc.title = "PRIVACY POLICY"
            self.navigationController?.pushViewController(vc, animated: true)
        case 2 :
            println(indexPath.row)
            var vc = self.storyboard?.instantiateViewControllerWithIdentifier("StaticViewController") as! StaticViewController
                vc.title = "TERMS AND CONDITIONS"
            self.navigationController?.pushViewController(vc, animated: true)
        case 3 :
            println(indexPath.row)
            var vc = self.storyboard?.instantiateViewControllerWithIdentifier("FeedbackViewController") as! FeedbackViewController
                vc.title = "FEEDBACK"
            self.navigationController?.pushViewController(vc, animated: true)
        case 4 :
            println(indexPath.row)
            var vc = self.storyboard?.instantiateViewControllerWithIdentifier("StaticViewController") as! StaticViewController
                vc.title = "ABOUT US"
            self.navigationController?.pushViewController(vc, animated: true)
        default :
            println("Access Denied")
        }
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
