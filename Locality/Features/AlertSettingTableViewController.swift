//
//  AlertSettingTableViewController.swift
//  Locality
//
//  Created by Thuc Truong on 6/29/15.
//  Copyright (c) 2015 peerbitVN. All rights reserved.
//

import UIKit
import Parse
class AlertSettingTableViewController: UITableViewController {
    @IBOutlet var btnArrStatus: [UIButton]!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "ALERT SETTINGS"
        let leftButton = UIBarButtonItem(image: UIImage(named: "close"), style: UIBarButtonItemStyle.Done, target: self, action: "pressed")
        leftButton.tintColor = UIColor.whiteColor()
        self.navigationItem.leftBarButtonItem = leftButton
        ///
        for bt in btnArrStatus {
            bt.selected = true
        }
        
        
        loadCurentStatus()
        
    }
    func pressed(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    var btnStatusArr = [false,false,true,false,false]
    func loadCurentStatus(){
        let Obj = PFQuery(className: "Incident")
        Obj.findObjectsInBackgroundWithBlock { (data :[AnyObject]?, error: NSError?) -> Void in
            if error == nil {
                for inci in data as! [PFObject] {
                    switch (inci["category"] as! String){ // tracking Type of Incident
                    case "Critical":
                        if (inci["status"] as! String) ==  "Active"{
                            self.btnStatusArr[0] = true
                        } else {
                            self.btnStatusArr[0] = false
                        }
                        
                    case "Emergency Services":
                        if (inci["status"] as! String) ==  "Active"{
                            self.btnStatusArr[1] = true
                        } else {
                            self.btnStatusArr[1] = false
                        }
                        
                    case "Travel":
                        if (inci["status"] as! String) ==  "Active"{
                            self.btnStatusArr[2] = true
                        } else {
                            self.btnStatusArr[2] = false
                        }
                        
                    case "Weather":
                        if (inci["status"] as! String) ==  "Active"{
                            self.btnStatusArr[3] = true
                        } else {
                            self.btnStatusArr[3] = false
                        }
                        
                    case "Events":
                        if (inci["status"] as! String) ==  "Active"{
                            self.btnStatusArr[4] = true
                        } else {
                            self.btnStatusArr[4] = false
                        }
                        
                    default:
                        println("------ACCESS DENIED------")
                    }
                    
                }
                for btn in self.btnArrStatus {
                    switch btn.tag {
                    case 0:
                        btn.selected = self.btnStatusArr[0]
                    case 1:
                         btn.selected = self.btnStatusArr[1]
                    case 2:
                         btn.selected = self.btnStatusArr[2]
                    case 3:
                         btn.selected = self.btnStatusArr[3]
                    case 4:
                         btn.selected = self.btnStatusArr[4]
                    default :
                        let alertController = UIAlertController(title: "Error", message:
                            "Fail setting btn!!", preferredStyle: UIAlertControllerStyle.Alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
                        self.presentViewController(alertController, animated: true, completion: nil)
                    }
                }
                
            } else {
                let alertController = UIAlertController(title: "Error", message:
                    "Fail!!! PFQuery", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            
            }
        }
        
        
    }


    @IBAction func touchbtnSw(sender: UIButton) {
        switch sender.tag {
        case 0:
            println("Click Critical")
            if sender.selected {
                //disable
                updateStatusOfincident("Critical", ExpectedStatus: "InActive")
                sender.selected = false
            } else {
                //enable
                updateStatusOfincident("Critical", ExpectedStatus: "Active")
                sender.selected = true
            }

        case 1:
            println("Click Emergency Services")
            if sender.selected {
                //disable
                updateStatusOfincident("Emergency Services", ExpectedStatus: "InActive")
                sender.selected = false
            } else {
                //enable
                updateStatusOfincident("Emergency Services", ExpectedStatus: "Active")
                sender.selected = true
            }
        case 2:
            println("Click Travel")
            if sender.selected {
                //disable
                updateStatusOfincident("Travel", ExpectedStatus: "InActive")
                sender.selected = false
            } else {
                //enable
                updateStatusOfincident("Travel", ExpectedStatus: "Active")
                sender.selected = true
            }
        case 3:
            println("Click Weather")
            if sender.selected {
                //disable
                updateStatusOfincident("Weather", ExpectedStatus: "InActive")
                sender.selected = false
            } else {
                //enable
                updateStatusOfincident("Weather", ExpectedStatus: "Active")
                sender.selected = true
            }
        case 4:
            println("Click Events")
            if sender.selected {
                //disable
                updateStatusOfincident("Events", ExpectedStatus: "InActive")
                sender.selected = false
            } else {
                //enable
                updateStatusOfincident("Events", ExpectedStatus: "Active")
                sender.selected = true
            }
        default:
            let alertController = UIAlertController(title: "Error", message:
                "Access Denied", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
    }
    func updateStatusOfincident(currentType:String,ExpectedStatus:String){
        let Obj = PFQuery(className: "Incident")
        Obj.findObjectsInBackgroundWithBlock { (data :[AnyObject]?, error: NSError?) -> Void in
            if error == nil {
                for inci in data as! [PFObject] {
                    if  (inci["category"] as! String) == currentType {
                        inci["status"] = ExpectedStatus
                        inci.saveInBackground()
                    }
                }
                
            } else {
                
                
            }
        }
    }
    @IBAction func touchAExample(sender: UIButton) {
        
        switch sender.tag {
        case 0:
            let alertController = UIAlertController(title: "Critical", message: "Traffic Accident " + "300 m from you", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        case 1:
            let alertController = UIAlertController(title: "Emergency Services", message: "Crime " + "300 m from you", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        case 2:
            let alertController = UIAlertController(title: "Travel", message: "Bus disruption " + "300 m from you", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        case 3:
            let alertController = UIAlertController(title: "Weather", message: "Dangerous road " + "300 m from you", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        case 4:
            let alertController = UIAlertController(title: "Events", message: "Traffic Jam" + "300 m from you", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        default:
            println(sender.tag)
        
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 5
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
